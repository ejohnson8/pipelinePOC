Feature: Quote generator

  Scenario: Fetch random quote

    Given url 'http://gitlab-hello-world-humble-crane.cfapps.io/quote'
    When method GET
    Then status 200
    And match $ contains "It's called reading"